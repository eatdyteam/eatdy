import React, { Component } from 'react';
import HeaderSection from '../header/HeaderSection/HeaderSection';
import Contact from '.';

class Contacts extends Component {
    render() {
        return (
            <div>
                <HeaderSection nameHD="Contact Us" nameHT="Contact Us"/>
                <Contact/>
            </div>
        );
    }
}

export default Contacts;