import React, { Component } from 'react';

class Contact extends Component {
    render() {
        return (
            <div>
                {/* start contact */}
                <div className="contact">
                <div className="container">
                <div className="row">
  
                    <div className="col-lg-6 col-md-6">
                        <div className="location_box">
                        <div className="location_icon"> <img className="img-responsive hoverin" src="/img/phone.png" alt="" /> <img className="img-responsive hoverout" src="img/phonehover.png" alt="" /> </div>
                        <div className="location_address">
                            <p><a href="Phone:">+84 32764 3253</a><br />
                            <a href="mailto:">linhhuacntt@gmail.com</a></p>
                        </div>
                        </div>
                    </div>
                    <div className="col-lg-6 col-md-6">
                        <div className="location_box">
                        <div className="location_icon"> <img className="img-responsive hoverin"  src="img/time.png" alt="" /> <img className="img-responsive hoverout" src="img/timehover.png" alt="" /> </div>
                        <div className="location_address">
                            <p>09:00am to 05:30pm<br />
                            Sunday Closed</p>
                        </div>
                        </div>
                    </div>
                </div>

                 </div>
                </div>
                {/* end contact */} 
                {/* start contact */}
                <div className="contact_form">
                <div className="container">
                    <div className="row">
                    <div className="col-lg-12">
                        <div className="title_box">
                        <h2>Get In Touch</h2>
                        <img className="img-fluid" src="img/title_line.png" /> </div>
                    </div>
                    <div className="offset-1 col-lg-10">
                        <form>
                        <div className="row">
                            <div className="col-lg-6">
                            <input type="text" className="contact_from" placeholder="First Name" required />
                            </div>
                            <div className="col-lg-6">
                            <input type="text" className="contact_from" placeholder="Last Name" required />
                            </div>
                            <div className="col-lg-6">
                            <input type="text" className="contact_from" placeholder="Phone no." required />
                            </div>
                            <div className="col-lg-6">
                            <input type="text" className="contact_from" placeholder="Email" required />
                            </div>
                            <div className="col-lg-12">
                            <textarea placeholder="Message..." className="contact_mes" required defaultValue={""} />
                            </div>
                            <div className="col-lg-12">
                            <div className="submit_btn1">
                                <button type="submit" className="submit_btn">Submit Now</button>
                            </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            
        );
    }
}

export default Contact;