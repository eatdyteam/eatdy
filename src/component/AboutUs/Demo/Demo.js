import React, { Component } from 'react';

class Demo extends Component {
    render() {
        return (
            <div id="testimonial" className="testimonials">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="row justify-content-center">
                                <div className="col-xs-12 col-md-8 col-lg-5 text-center">
                                    <div className="slider slider-nav slick-initialized slick-slider">
                                        <div aria-live="polite" className="slick-list draggable" style={{padding: '0px 5px'}}><div className="slick-track" role="listbox" style={{opacity: 1, width: '1885px', transform: 'translate3d(-1015px, 0px, 0px)'}}>
                                            <div className="client-thumbnail slick-slide slick-cloned" style={{width: '145px'}} data-slick-index={-4} aria-hidden="true" tabIndex={-1}>
                                                <img src="img/linh.jpg" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide slick-cloned" style={{width: '145px'}} data-slick-index={-3} aria-hidden="true" tabIndex={-1}>
                                                <img src="https://scontent.fdad3-3.fna.fbcdn.net/v/t1.0-9/41408177_1197558613724901_4253242809705824256_n.jpg?_nc_cat=109&_nc_ohc=cKGId7qGcFgAX_wu01D&_nc_ht=scontent.fdad3-3.fna&oh=ff34dadea7602cf8577f665a6989409d&oe=5E9041CF" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide slick-cloned" style={{width: '145px'}} data-slick-index={-2} aria-hidden="true" tabIndex={-1}>
                                                <img src="img/linh.jpg" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide slick-cloned slick-center" style={{width: '145px'}} data-slick-index={-1} aria-hidden="true" tabIndex={-1}>
                                                <img src="ihttps://scontent.fdad3-3.fna.fbcdn.net/v/t1.0-9/41408177_1197558613724901_4253242809705824256_n.jpg?_nc_cat=109&_nc_ohc=cKGId7qGcFgAX_wu01D&_nc_ht=scontent.fdad3-3.fna&oh=ff34dadea7602cf8577f665a6989409d&oe=5E9041CF" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide" style={{width: '145px'}} data-slick-index={0} aria-hidden="true" tabIndex={-1} role="option" aria-describedby="slick-slide10">
                                                <img src="img/linh.jpg" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide" style={{width: '145px'}} data-slick-index={1} aria-hidden="true" tabIndex={-1} role="option" aria-describedby="slick-slide11">
                                                <img src="https://scontent.fdad3-3.fna.fbcdn.net/v/t1.0-9/41408177_1197558613724901_4253242809705824256_n.jpg?_nc_cat=109&_nc_ohc=cKGId7qGcFgAX_wu01D&_nc_ht=scontent.fdad3-3.fna&oh=ff34dadea7602cf8577f665a6989409d&oe=5E9041CF" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide" style={{width: '145px'}} data-slick-index={2} aria-hidden="true" tabIndex={-1} role="option" aria-describedby="slick-slide12">
                                                <img src="img/linh.jpg" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide slick-active" style={{width: '145px'}} data-slick-index={3} aria-hidden="false" tabIndex={-1} role="option" aria-describedby="slick-slide13">
                                                <img src="https://scontent.fdad3-3.fna.fbcdn.net/v/t1.0-9/41408177_1197558613724901_4253242809705824256_n.jpg?_nc_cat=109&_nc_ohc=cKGId7qGcFgAX_wu01D&_nc_ht=scontent.fdad3-3.fna&oh=ff34dadea7602cf8577f665a6989409d&oe=5E9041CF" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide slick-current slick-active slick-center" style={{width: '145px'}} data-slick-index={4} aria-hidden="false" tabIndex={-1} role="option" aria-describedby="slick-slide14">
                                                <img src="img/linh.jpg" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide slick-cloned slick-active" style={{width: '145px'}} data-slick-index={5} aria-hidden="false" tabIndex={-1}>
                                                <img src="https://scontent.fdad3-3.fna.fbcdn.net/v/t1.0-9/41408177_1197558613724901_4253242809705824256_n.jpg?_nc_cat=109&_nc_ohc=cKGId7qGcFgAX_wu01D&_nc_ht=scontent.fdad3-3.fna&oh=ff34dadea7602cf8577f665a6989409d&oe=5E9041CF" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide slick-cloned" style={{width: '145px'}} data-slick-index={6} aria-hidden="true" tabIndex={-1}>
                                                <img src="img/linh.jpg" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide slick-cloned" style={{width: '145px'}} data-slick-index={7} aria-hidden="true" tabIndex={-1}>
                                                <img src="https://scontent.fdad3-3.fna.fbcdn.net/v/t1.0-9/41408177_1197558613724901_4253242809705824256_n.jpg?_nc_cat=109&_nc_ohc=cKGId7qGcFgAX_wu01D&_nc_ht=scontent.fdad3-3.fna&oh=ff34dadea7602cf8577f665a6989409d&oe=5E9041CF" alt="BWmedia" className="img-fluid" />
                                            </div>
                                            <div className="client-thumbnail slick-slide slick-cloned" style={{width: '145px'}} data-slick-index={8} aria-hidden="true" tabIndex={-1}>
                                                <img src="img/linh.jpg" alt="BWmedia" className="img-fluid" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-12 col-md-10 col-lg-8">
                                <div className="slider slider-for slick-initialized slick-slider"><span className="slick-arrow" style={{}}><i className="fa fa-angle-left" /></span>
                                    <div aria-live="polite" className="slick-list draggable">
                                        <div className="slick-track" style={{opacity: 1, width: '3650px'}} role="listbox">
                                            <div className="client-feedback-text text-center slick-slide" data-slick-index={0} aria-hidden="true" tabIndex={-1} role="option" aria-describedby="slick-slide00" style={{width: '730px', position: 'relative', left: '0px', top: '0px', zIndex: 998, opacity: 0, transition: 'opacity 500ms ease 0s'}}>
                                                <div className="client-name text-center">
                                                    <h5>Hứa Thị Linh</h5>
                                                    <h6>Beautiful, Welcoming Web-App Eatdy!</h6>
                                                </div>
                                                <div className="testimonials-content">
                                                    <p>A meal full of nutrients and provides enough energy but still keep the body you want. Eatdy is always with you wherever you are, advising you on a diet that suits you and always reminds you if you forget. Don't worry about losing shape, don't worry about lack of quality, ... because you've got Eatdy.</p>
                                                </div>
                                             </div>
                                            <div className="client-feedback-text text-center slick-slide" data-slick-index={1} aria-hidden="true" tabIndex={-1} role="option" aria-describedby="slick-slide01" style={{width: '730px', position: 'relative', left: '-730px', top: '0px', zIndex: 998, opacity: 0, transition: 'opacity 500ms ease 0s'}}>
                                                <div className="client-name text-center">
                                                    <h5>Nguyễn Đắc Chí</h5>
                                                    <h6>Beautiful, Welcoming Web-App Eatdy!</h6>
                                                </div>
                                                <div className="testimonials-content">
                                                    <p>A meal full of nutrients and provides enough energy but still keep the body you want. Eatdy is always with you wherever you are, advising you on a diet that suits you and always reminds you if you forget. Don't worry about losing shape, don't worry about lack of quality, ... because you've got Eatdy.</p>
                                                </div>
                                            </div>
                                            <div className="client-feedback-text text-center slick-slide" data-slick-index={2} aria-hidden="true" tabIndex={-1} role="option" aria-describedby="slick-slide02" style={{width: '730px', position: 'relative', left: '-1460px', top: '0px', zIndex: 998, opacity: 0, transition: 'opacity 500ms ease 0s'}}>
                                                <div className="client-name text-center">
                                                    <h5>Hứa Thị Linh</h5>
                                                    <h6>Beautiful, Welcoming Web-App Eatdy!</h6>
                                                </div>
                                                <div className="testimonials-content">
                                                    <p>A meal full of nutrients and provides enough energy but still keep the body you want. Eatdy is always with you wherever you are, advising you on a diet that suits you and always reminds you if you forget. Don't worry about losing shape, don't worry about lack of quality, ... because you've got Eatdy.</p>
                                                </div>
                                            </div>
                                            <div className="client-feedback-text text-center slick-slide" data-slick-index={3} aria-hidden="true" tabIndex={-1} role="option" aria-describedby="slick-slide03" style={{width: '730px', position: 'relative', left: '-2190px', top: '0px', zIndex: 998, opacity: 0, transition: 'opacity 500ms ease 0s'}}>
                                                <div className="client-name text-center">
                                                    <h5>Nguyễn Đắc Chí</h5>
                                                    <h6>Beautiful, Welcoming Web-App Eatdy!</h6>
                                                </div>
                                                <div className="testimonials-content">
                                                    <p>A meal full of nutrients and provides enough energy but still keep the body you want. Eatdy is always with you wherever you are, advising you on a diet that suits you and always reminds you if you forget. Don't worry about losing shape, don't worry about lack of quality, ... because you've got Eatdy.</p>
                                                </div>
                                            </div>
                                            <div className="client-feedback-text text-center slick-slide slick-current slick-active" data-slick-index={4} aria-hidden="false" tabIndex={-1} role="option" aria-describedby="slick-slide04" style={{width: '730px', position: 'relative', left: '-2920px', top: '0px', zIndex: 999, opacity: 1}}>
                                                <div className="client-name text-center">
                                                    <h5>Hứa Thị Linh</h5>
                                                    <h6>Beautiful, Welcoming Web-App Eatdy!</h6>
                                                </div>
                                                <div className="testimonials-content">
                                                    <p>A meal full of nutrients and provides enough energy but still keep the body you want. Eatdy is always with you wherever you are, advising you on a diet that suits you and always reminds you if you forget. Don't worry about losing shape, don't worry about lack of quality, ... because you've got Eatdy.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span className="slick-arrow" style={{}}><i className="fa fa-angle-right" /></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Demo;