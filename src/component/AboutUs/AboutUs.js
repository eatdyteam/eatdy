import React, { Component } from 'react';
// import Demo from '../Home/Demo'
import AboutEatdy from './AboutEatdy'
import WhatWeDo from './WhatWeDo'
import Demo from './Demo/Demo';
import HeaderSection from './../header/HeaderSection/HeaderSection';


class AboutUs extends Component {
    render() {
        return (
            <div>
                <HeaderSection nameHD="About Us" nameHT="About Us"/>
                <AboutEatdy/>
                <Demo/>
                <WhatWeDo/>
            </div>
        );
    }
}

export default AboutUs;