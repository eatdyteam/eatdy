import React, { Component } from 'react';
import Title from '../../Title'

class AboutEatdy extends Component {
    render() {
        return (
            <div className="about_us">
                <div className="container">
                    <div className="row">
                    <Title title="About eatdy"/>
                    </div>
                    <div className="row">
                    <div className="col-lg-6">
                        <div className="about_con">
                        <h3>A healthy outside starts from the inside</h3>
                        <p>Food is an essential need for health, it is more important than clothes and shelter. Food brings nutrition and is the ‘fuel’ essential to operating the body’s functions. But how many of us know the importance of good food? A good relationship with food means you understand exactly time, the way we eat and what kinds of food to improve your health.</p>
                        <p>nfortunately, people are either too perfectionist about their bodies without paying any attention to properly supplementing food while others use unlimited food that leads to excess. History has proven that, besides nutrients, food is an indispensable part to maintain human development.</p>
                        <div className="about_btn"> <a href="#" className="aboutbtn">Read More</a> </div>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="about_img"> <img className="img-fluid" src="img/about1.jpg" /> </div>
                        <div className="about_img_1"> <img className="img-fluid" src="img/about2.jpg" />
                        <div className="overlay">
                            <h1 className="headline"> Body,
                            Beauty,
                            Health. </h1>
                            <span className="description"> The diet is healthy, but it still keeps the body you want. </span> </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default AboutEatdy;