import React, { Component } from 'react';
import Title from '../../Title'

class WhatWeDo extends Component {
    render() {
        return (
            <div className="what_we_do">
            <div className="container">
              <div className="row">
                  <Title title="What we do"/>
                </div>
              <div className="row">
                <div className="col-lg-6">
                  <div className="about_con">
                    <h3>Yoga enhances your life.</h3>
                    <p>Eatdy is your companion wherever you are. Advise you a daily menu that suits your requirements and keep a body shape as you desire to scientifically ensure the necessary nutrients to provide energy for the long working day.</p>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="about_img"> <img className="img-fluid" src="img/TĐ-chay.jpg" /> </div>
                  <div className="about_img1" ><img className="img-fluid" src="img/imagess.jpg" />
                    <div className="about_img_2"> <img className="img-fluid" src="img/TĐ-man.jpg" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        );
    }
}

export default WhatWeDo;