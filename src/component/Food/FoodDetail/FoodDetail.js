import React, { Component } from 'react';
import NewItem from '../../News/NewItem';
import Title from '../../Title';

class FoodDetail extends Component {
    render() {
        return (
            <div className="instructors">
                <div className="container">
                    <div className="row">
                        <Title title="Food"/>
                        <div className="col-lg-12">
                            <div className="owl-carousel owl-theme ss_carousel owl-loaded owl-drag" id="carousel">
                                <div className="owl-stage-outer">
                                    <div className="owl-stage" style={{transform: 'translate3d(-1080px, 0px, 0px)', transition: 'all 0s ease 0s', width: '3510px'}}>
                                        <NewItem nameItem="Foods" textItem="For people gym" linkItem="https://genvita.vn/bai-bao/an-gi-truoc-khi-tap-gym-kham-pha-5-mon-don-gian-nhung-hieu-qua-bat-ngo" imgItem="img/food-1.png"/>
                                        <NewItem nameItem="Foods" textItem="For people gym" linkItem="https://www.wheystore.vn/bua-an-chuan-cho-nguoi-tap-gym-1373.html" imgItem="img/food2.jpg"/>
                                        <NewItem nameItem="Foods" textItem="For people to gain weight" linkItem="http://kenh14.vn/nguoi-gay-muon-tang-can-hay-an-nhung-mon-nay-20180212105006898.chn" imgItem="img/food3.jpg"/>
                                        <NewItem nameItem="Foods" textItem="For people losing weight" linkItem="https://genvita.vn/bai-bao/huong-dan-thuc-hanh-phuong-phap-giam-can-keto-cho-nguoi-moi-bat-dau" imgItem="img/food4.jpg"/>
                                        <NewItem nameItem="Foods" textItem="For people yoga" linkItem="http://www.yogaplus.vn/blog/7-thuc-pham-nen-an-khi-tap-yoga" imgItem="img/food5 .jpg"/>
                                        <NewItem nameItem="Foods" textItem="For people yoga" linkItem="https://www.citigym.com.vn/tin-tuc/tin-tuc/blog/an-uong/an-uong/nhung-nguyen-tac-vang-trong-dinh-dung-cho-nguoi-tap-yoga" imgItem="img/food6.jpg"/>
                                        <NewItem nameItem="Foods" textItem="For people to lossing weight" linkItem="https://2monngonmoingay.com/toi-da-giam-can-thanh-cong-nho-an-kieng-voi-cac-mon-an-tu-dau-phu.html" imgItem="img/food7.jpg"/>
                                        <NewItem nameItem="Foods" textItem="For people to lossing weight" linkItem="http://giammobung.online/che-do-an-giam-beo-danh-cho-nguoi-ban-ron/" imgItem="img/food8.jpg"/>
                                        <NewItem nameItem="Foods" textItem="For people to lossing weight" linkItem="https://ybspa.vn/yb-spa-riview-bi-quyet-giam-can" imgItem="img/food9.jpg"/>
                                        <NewItem nameItem="Foods" textItem="For people to lossing weight" linkItem="http://giadinh.net.vn/song-khoe/ap-dung-10-thoi-quen-an-toi-nay-ban-se-giam-can-mot-cach-de-dang-20180109154946155.htm" imgItem="img/food10.jpg"/>
                                        
                                    </div>
                                </div>
                                    <div className="owl-nav disabled">
                                        <div className="owl-prev">
                                            <i className="fa fa-angle-left" />
                                        </div>
                                        <div className="owl-next">
                                            <i className="fa fa-angle-right" />
                                        </div>
                                    </div>
                                        <div className="owl-dots">
                                            <div className="owl-dot active">
                                                <span />
                                            </div>
                                            <div className="owl-dot">
                                                <span />
                                            </div>
                                            
                                        </div>
                                    </div>

                                </div> 
                            </div>
                        </div>
                    </div>

        );
    }
}

export default FoodDetail;