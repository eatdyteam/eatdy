import React, { Component } from 'react';
import HeaderSection from '../header/HeaderSection/HeaderSection';
import FoodDetail from './FoodDetail/FoodDetail';

class Food extends Component {
    render() {
        return (
            <div>
                <HeaderSection nameHD="Food" nameHT="Food"/>
                <FoodDetail/>
            </div>
        );
    }
}

export default Food;
