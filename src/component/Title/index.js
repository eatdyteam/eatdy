import React, { Component } from 'react';

class Title extends Component {
    render() {
        return (
               <div className="col-lg-12">
                    <div className="title_box">
                        <h2>{this.props.title}</h2>
                        <img className="img-fluid" src="img/title_line.png" /> 
                    </div>
                </div>
        );
    }
}

export default Title;