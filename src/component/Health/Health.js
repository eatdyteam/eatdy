import React, { Component } from 'react';
import HeaderSection from '../header/HeaderSection/HeaderSection';
import HealthNew from '.';

class Health extends Component {
    render() {
        return (
            <div>
                <HeaderSection nameHD="Health" nameHT="Health"/>
                <HealthNew />
            </div>
        );
    }
}

export default Health;