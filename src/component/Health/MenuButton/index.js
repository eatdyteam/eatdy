import React, { Component } from 'react';
import MenuButtonItem from './MenuButtonItem';

class MenuButton extends Component {
    render() {
        return (
                <div className="link_box">
                    <MenuButtonItem nameButton="All"/>
                    <MenuButtonItem nameButton="Food"/>
                    <MenuButtonItem nameButton="Gym"/>
                    <MenuButtonItem nameButton="Yoga"/>
                    <MenuButtonItem nameButton="Diet"/>
                 </div>
        );
    }
}

export default MenuButton;