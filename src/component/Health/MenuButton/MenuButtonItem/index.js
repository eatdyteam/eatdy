import React, { Component } from 'react';

class MenuButtonItem extends Component {
    render() {
        return (
            <button className="btn btn-default filter-button" data-filter="all">{this.props.nameButton}</button>
        );
    }
}

export default MenuButtonItem;