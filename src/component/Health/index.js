import React, { Component } from 'react';
import MenuButton from './MenuButton'
import HealthNewItem from './HealthNewItem';
import Title from '../Title';

class HealthNew extends Component {
    render() {
        return (

            <div className="choose_class">
                <div className="container">
                    <div className="row">
                    <Title title="Health News"/>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <MenuButton/>
                        <br />
                        <HealthNewItem imgHealth="img/new_1.jpg" nameHealth="Dirty food"/>
                        <HealthNewItem imgHealth="img/new_2.jpg" nameHealth="Food"/>
                        <HealthNewItem imgHealth="img/new_4.jpg" nameHealth="Gym"/>
                        <HealthNewItem imgHealth="img/new_5.jpg" nameHealth="Diet"/>
                        <HealthNewItem imgHealth="img/new6.jpg" nameHealth="Diet"/>
                        <HealthNewItem imgHealth="img/new_3.jpg" nameHealth="Yoga"/>
                    </div>
                </div>
            </div>

        );
    }
}

export default HealthNew;