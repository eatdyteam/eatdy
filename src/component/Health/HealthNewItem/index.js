import React, { Component } from 'react';

class HealthNewItem extends Component {
    render() {
        return (
            <div className="gallery_product col-lg-4 col-md-6 col-sm-4 col-xs-6 filter food">
                <div className="portfolio_box">
                    <div className="portfolio_img"> <img className="img-fluid" src={this.props.imgHealth} /> </div>
                    <div className="portfolio_con">
                    <div className="add_sign"> <a href="#" className="add_sign_btn"><i className="fa fa-plus" aria-hidden="true" /></a> </div>
                    <div className="portfolio_con space">
                        <h4>News</h4>
                        <h3>{this.props.nameHealth}</h3>
                    </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default HealthNewItem;