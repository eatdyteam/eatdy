import React, { Component } from 'react';

class HeaderSection extends Component {
    render() {
        return (
            <div id="demo1" className="carousel slide" data-ride="carousel">
                <div className="carousel-inner">
                    <div className="carousel-item active" style={{backgroundImage: 'url(assets/img/about_bg.png)', height: '526px'}}>
                    <div className="slider_text1">
                        <h1>{this.props.nameHD}</h1>
                    </div> 
                    <div className="carousel-caption1">
                        <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                            <div className="inner_page_title">
                                <h2>{this.props.nameHT}</h2>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default HeaderSection;