import React, { Component } from 'react';
import * as ROUTES from "../../../constants/routes";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

class TopHeader extends Component {
    render() {
        return (
            <div>
                <header>
                    <div className="container">
                        <div className="row">
                        <div className="col-lg-12">
                            <nav className="main-nav" role="navigation"> 
                            {/* Mobile menu toggle button (hamburger/x icon) */}
                            <input id="main-menu-state" type="checkbox" />
                            <label className="main-menu-btn" htmlFor="main-menu-state"> <span className="main-menu-btn-icon" /> Toggle main menu visibility </label>
                            <h2 className="nav-brand"><a href="index.html" style={{fontSize: '56px', color: 'white', paddingTop: '18px', fontFamily: 'fantasy', fontWeight: 'lighter'}} className="logo">eatdy</a></h2>
                            {/* Sample menu definition */}
                            <ul id="main-menu" className="sm sm-clean" data-smartmenus-id={1559366433783545}>
                                <li><Link to={ROUTES.HOME}>Home</Link></li>
                                <li><Link to={ROUTES.ABOUT}>About Us</Link></li>
                                <li><Link to={ROUTES.NEW}>News</Link></li>
                                <li><Link to={ROUTES.FOOD}>Food</Link></li>
                                <li><Link to={ROUTES.HEALTH}>Health</Link></li>
                                <li><Link to={ROUTES.SIGN_IN}>Login</Link></li>
                                <li className="last_menu"><Link to={ROUTES.CONTACT}>Contact</Link></li>
                            </ul>
                            </nav>
                        </div>
                        </div>
                    </div>
                    </header>
                </div>
        );
    }
}

export default TopHeader;