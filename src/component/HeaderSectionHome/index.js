import React, { Component } from 'react';
import * as ROUTES from "../../constants/routes";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

class HeaderSectionHome extends Component {
    render() {

        return (
           
            <div id="demo"  className="carousel slide" data-ride="carousel">
                <div className="carousel-inner">
                    <div className="carousel-item active" style={{backgroundImage: 'url("img/bua-removebg-preview.png")', backgroundPosition: 'top', backgroundRepeat: 'no-repeat', backgroundSize: 'contain'}}>
                        <div className="slider_text">
                            <h1>The first wealth is health</h1>
                        </div> 
                        <div className="carousel-caption">	
                            <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="carousel-caption1s " style={{height: '466px'}}>
                                        <h3 style={{textAlign: 'center', fontSize: '70px'}}>The first wealth 
                                        is health</h3>
                                        <p style={{fontSize: '21px', fontFamily: 'cursive'}}>Most illnesses do not, as is generally thought, come like a bolt out of the blue.
                                        The ground is prepared for years through faulty diet, 
                                        intemperance, overwork, and moral conflicts, slowly eroding the subject's vitality.</p>
                                        <div className="slider_btn" style={{textAlign: 'center', fontSize: '53px'}}> <Link to ={ROUTES.SIGN_IN} className="sliderbtn">Eat now</Link> </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        );
    }
}

export default HeaderSectionHome;