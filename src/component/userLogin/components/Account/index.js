import React, { Component } from 'react';
import * as ROUTES from '../../../../constants/routes';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./index.css";
import Item from './FoodItem/Item'

import {
  AuthUserContext,
  withAuthorization,
  withEmailVerification,
} from '../Session';
import { withFirebase } from '../../../../Firebase';
import { PasswordForgetForm } from '../PasswordForget';
import PasswordChangeForm from '../PasswordChange';
import './index.css'
import Title from "../../../Title";






class AccountPage extends Component {

  constructor(props) {
    super(props);
    this.state = {

        menu:[],
        ngay :1,
        id:''
    }
} 

      getId =(id)=>{
        this.setState({
          id:id
        });
      }
      // componentDidUpdate() {
      //   var menus = [];
      //   this.props.firebase.menu(this.props.uid).on('value', (list) => {
      //       list.forEach(element => {
   
      //           menus.push(element.val());
      //       });
      //       this.setState(
      //           {
      //               menu: menus
      //           })

      //           console.log(this.state.menu);
      //   });
      
      // }
      
     
   
      render() {

        return (
          <div>
          <AuthUserContext.Consumer>
          {authUser => (

            
            <div>
            <div className="about_us">
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  <div className="title_box">
                    <h2 id="hi">Hello<h2 id="nameUser">{authUser.username}</h2></h2>
                    <img className="img-fluid" src="img/title_line.png" />
                  </div>
                </div>
              </div>
            </div>
          </div>
      <div className="container emp-profile">
        <form method="post">
          <div className="row">
            <div className="col-md-4">
              <div className="profile-img">
                <img src="https://scontent.fdad3-1.fna.fbcdn.net/v/t1.0-9/72256652_774549133002867_1748417036570066944_n.jpg?_nc_cat=106&_nc_ohc=frmExb39BMwAX_Skads&_nc_ht=scontent.fdad3-1.fna&oh=93622369ca8dab562d311e86b3407ee6&oe=5EAEFDEB" alt="" />
                <div className="file btn btn-lg btn-primary">
                  Change Photo
            <input type="file" name="file" />
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="profile-head">
                <h5>
                  {authUser.username  }
          </h5>
                <h6>
                  Web Developer and Designer
          </h6>
                <p className="proile-rating">RANKINGS : <span>8/10</span></p>
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                  <li className="nav-item">
                    <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Timeline</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md-2">
              <Link to ={ROUTES.INFORMATION} type="button"className="profile-edit-btn" name="btnAddMore" >Edit Profile</Link>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="profile-work">
                <p>WORK LINK</p>
                <a href>Website Link</a><br />
                <a href>Bootsnipp Profile</a><br />
                <a href>Bootply Profile</a>
                <p>SKILLS</p>
                <a href>Web Designer</a><br />
                <a href>Web Developer</a><br />
                <a href>WordPress</a><br />
                <a href>WooCommerce</a><br />
                <a href>PHP, .Net</a><br />
              </div>
            </div>
            <div className="col-md-8">
              <div className="tab-content profile-tab" id="myTabContent">
                <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  <div className="row">
                    <div className="col-md-6">
                      <label>User Id</label>
                    </div>
                    <div className="col-md-6">
                      <p>{authUser.uid}</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Name</label>
                    </div>
                    <div className="col-md-6">
                      <p>{authUser.username}</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Email</label>
                    </div>
                    <div className="col-md-6">
                      <p>{authUser.email}</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Phone</label>
                    </div>
                    <div className="col-md-6">
                      <p>123 456 7890</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Profession</label>
                    </div>
                    <div className="col-md-6">
                      <p>Web Developer and Designer</p>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  <div className="row">
                    <div className="col-md-6">
                      <label>Experience</label>
                    </div>
                    <div className="col-md-6">
                      <p>Expert</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Hourly Rate</label>
                    </div>
                    <div className="col-md-6">
                      <p>10$/hr</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Total Projects</label>
                    </div>
                    <div className="col-md-6">
                      <p>230</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>English Level</label>
                    </div>
                    <div className="col-md-6">
                      <p>Expert</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label>Availability</label>
                    </div>
                    <div className="col-md-6">
                      <p>6 months</p>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <label>Your Bio</label><br />
                      <p>Your detail description</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      <Item uid={authUser.uid}
      menus={authUser.menus}
      chedo={authUser.chedo}></Item>
      </div>
      <div>

      </div>
      {/* <h1>{authUser.menus}</h1> */}
      </div>
      
        
        
      )}
    </AuthUserContext.Consumer>
  </div>

           
        )
      }
    }

    export default withFirebase(AccountPage);