import React, { Component } from 'react';
import * as ROUTES from '../../../../../constants/routes';
import { Link } from 'react-router-dom';
class ItemTwo extends Component {
  
    
    
    render() {
  
        
        return (
            
            <div className="owl-item cloned" style={{width: '350px'}}>
                <div className="item"><div className="item1">
                    <div className="team-img"> 
                    <img src={this.props.hinhanh}  className="img-fluid" />
                    <div className="team-bottom-box">
                        <div className="bottom-team-border">
                            <h2>Foods</h2>
                            <p>{this.props.ten}</p>
                            <h4><Link to ={ROUTES.MAP}>Địa Chỉ</Link></h4>
                            <h4><a href={this.props.cachlam}>Cách làm</a></h4>
                            </div></div></div></div></div></div>
        );
    }
}

export default ItemTwo;