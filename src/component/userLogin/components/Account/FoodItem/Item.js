import React, { Component } from 'react';
import Title from "../../../../Title";
import { withFirebase } from '../../../../../Firebase';
import ItemTwo from "../Item";
import { thisExpression } from '@babel/types';

class Item extends Component {

    constructor(props) {
        super(props);
        this.state = {

            menu: [],

            isLoading: false,
            ngay1: [],
            ngay2: [],
            menu: this.props.menu

        }
    }

    componentWillMount() {
        this.getDataFromDB();
        this.getData1();
    }

    getDataFromDB() {
        var menu = [];
        this.props.firebase.menu(this.props.uid).on('value', (list) => {
            list.forEach(element => {
                menu.push(element.val());
            });
            this.setState(
                {
                    menu: menu
                })
        });
    }


    getData1 = () => {
        if (this.props.menus) {
            this.props.menus.forEach(item => {
                if (item.buoi == "SANG") {
                    this.setState({
                        SANG1: item
                    });
                }
                if (item.buoi == "TRUA") {
                    this.setState({
                        TRUA1: item
                    })
                }
                if (item.buoi == "TOI") {
                    this.setState({
                        TOI1: item
                    })
                }
            })
            this.props.menus.forEach(element => {

                if (element.buoi == "SANG" && element != this.state.SANG1) {
                    this.setState({
                        SANG2: element
                    });
                }
                if (element.buoi == "TRUA" && element != this.state.TRUA1) {
                    this.setState({
                        TRUA2: element
                    })
                }
                if (element.buoi == "TOI" && element != this.state.TOI1) {
                    this.setState({
                        TOI2: element
                    })
                }
            })
        }
        // this.props.menus.forEach(element => {
        //     if (element.buoi == "SANG") {
        //         this.setState({
        //             SANG1: element
        //         });
        //     }
        //     if (element.buoi == "TRUA") {
        //         this.setState({
        //             TRUA1: element
        //         })
        //     }
        //     if (element.buoi == "TOI") {
        //         this.setState({
        //             TOI1: element
        //         })
        //     }
        // })

    }
    setmenu = () => {

        var ngay1 = [];
        var ngay2 = [];
        var thucdon = {};
        var menu = this.props.menus;

        var lenght = menu.lenght;
        for (var i = 0; i < menu.lenght; i++) {
            // menu.forEach(element=>{

            if (menu[i].buoi == "SANG") {

                thucdon = menu[i];
                menu.remove(i);

            }
        }


        for (var i = 0; i < menu.lenght; i++) {
            if (menu[i].buoi == "TRUA") {
                ngay1.push(menu[i]);
                menu.remove(menu[i]); break;
            }
        }
        for (var i = 0; i < menu.lenght; i++) {
            if (menu[i].buoi == "TOI") {
                ngay1.push(menu[i]); menu.remove(menu[i]); break;
            }
        }

        for (var i = 0; i < menu.lenght; i++) {
            if (menu[i].buoi == "SANG") {
                this.state.ngay2.push(menu[i]);
                menu.remove(menu[i]); break;
            }
        }
        for (var i = 0; i < menu.lenght; i++) {
            if (menu[i].buoi == "TRUA") {
                this.state.ngay2.push(menu[i]);
                menu.remove(menu[i]); break;
            }
        }
        for (var i = 0; i < menu.lenght; i++) {
            if (menu[i].buoi == "TOI") {
                this.state.ngay2.push(menu[i]);
                menu.remove(menu[i]); break;
            }
        }
        this.setState({
            ngay1: ngay1
        });

    }


    getMenu = (id) => {
        console.log(this.props.menus);
        
 
        if (this.props.menus) {
            console.log(this.props.menus);
            
            console.log(this.props.menus[id]);
            
            return <div>
                <ItemTwo
                    ten={this.props.menus[id].m1.ten}
                    cachlam={this.props.menus[id].m1.cachlam}
                    hinhanh={this.props.menus[id].m1.hinhanh}
                />
                <ItemTwo
                    ten={this.props.menus[id].m2.ten}
                    cachlam={this.props.menus[id].m2.cachlam}
                    hinhanh={this.props.menus[id].m2.hinhanh}
                />
                <ItemTwo
                    ten={this.props.menus[id].m3.ten}
                    cachlam={this.props.menus[id].m3.cachlam}
                    hinhanh={this.props.menus[id].m3.hinhanh}
                />
            </div>
        }

    }
    render() {




        return (
            <div className="instructors">
                <div className="container">
                    <div className="row">
                        <Title title="Buổi Sáng" />
                        <div className="col-lg-12">
                            <div className="owl-carousel owl-theme ss_carousel owl-loaded owl-drag" id="carousel">
                                <div className="owl-stage-outer">
                                    <div className="owl-stage" style={{ transform: 'translate3d(-1080px, 0px, 0px)', transition: 'all 0s ease 0s', width: '3510px' }}>


                                    </div>
                                </div>
                                <div className="owl-nav disabled">
                                    <div className="owl-prev">
                                        <i className="fa fa-angle-left" />
                                    </div>
                                    <div className="owl-next">
                                        <i className="fa fa-angle-right" />
                                    </div>
                                </div>
                                {this.getMenu(0)}

                                <div className="owl-dots">
                                    <div className="owl-dot active">
                                        <span />
                                    </div>
                                    <div className="owl-dot">
                                        <span />
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div>
                    <div className="container">
                        <div className="row">
                            <Title title="Buổi Trưa" />
                            <div className="col-lg-12">
                                <div className="owl-carousel owl-theme ss_carousel owl-loaded owl-drag" id="carousel">
                                    <div className="owl-stage-outer">
                                        <div className="owl-stage" style={{ transform: 'translate3d(-1080px, 0px, 0px)', transition: 'all 0s ease 0s', width: '3510px' }}>


                                        </div>
                                    </div>
                                    <div className="owl-nav disabled">
                                        <div className="owl-prev">
                                            <i className="fa fa-angle-left" />
                                        </div>
                                        <div className="owl-next">
                                            <i className="fa fa-angle-right" />
                                        </div>
                                    </div>
                                    {this.getMenu(1)}

                                    <div className="owl-dots">
                                        <div className="owl-dot active">
                                            <span />
                                        </div>
                                        <div className="owl-dot">
                                            <span />
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row">
                            <Title title="Buổi Tối" />
                            <div className="col-lg-12">
                                <div className="owl-carousel owl-theme ss_carousel owl-loaded owl-drag" id="carousel">
                                    <div className="owl-stage-outer">
                                        <div className="owl-stage" style={{ transform: 'translate3d(-1080px, 0px, 0px)', transition: 'all 0s ease 0s', width: '3510px' }}>


                                        </div>
                                    </div>
                                    <div className="owl-nav disabled">
                                        <div className="owl-prev">
                                            <i className="fa fa-angle-left" />
                                        </div>
                                        <div className="owl-next">
                                            <i className="fa fa-angle-right" />
                                        </div>
                                    </div>
                                    {this.getMenu(2)}

                                    <div className="owl-dots">
                                        <div className="owl-dot active">
                                            <span />
                                        </div>
                                        <div className="owl-dot">
                                            <span />
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div>

                    </div>
                </div>
            </div>

        );
    }
}

export default withFirebase(Item);