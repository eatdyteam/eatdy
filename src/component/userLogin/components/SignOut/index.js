import React from 'react';
import * as ROUTES from '../../../../constants/routes';
import { withFirebase } from '../../../../Firebase';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const SignOutButton = ({ firebase }) => (
  <Link to ={ROUTES.HOME} onClick={firebase.doSignOut}> Sign Out</Link>
);

export default withFirebase(SignOutButton);
