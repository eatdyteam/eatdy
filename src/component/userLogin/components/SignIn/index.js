import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { withFirebase } from '../../../../Firebase';
import * as ROUTES from '../../../../constants/routes';
import "./signin.css";
import { Link } from 'react-router-dom';
import "../../font-awesome/4.7.0/css/font-awesome.min.css"

const SignInPage = () => (
  <div>
    <div className="about_us">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="title_box">
              <h2>LoGin</h2>
              <img className="img-fluid" src="img/title_line.png" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div className="form_wrapper">
      <div className="form_container">
        <div className="row clearfix">
          <div className="col_half last">
            <SignInForm />
            <div className="row clearfix bottom_row">
              <input name type="checkbox" defaultValue /> Remember me
            <div className="col_half forgot_pw"><Link to={ROUTES.PASSWORD_FORGET} >Forgot Password?</Link></div>
            </div>
          </div>
          <div className="col_half">
            <SignInGoogle />
            <SignInFacebook />
            <SignInTwitter />
            <Link to={ROUTES.SIGN_UP}><div className="social_btn new"><a href="#"><span><i className="fa fa-plus-circle" aria-hidden="true" />
            </span>Create New Acount</a></div></Link>

          </div>
        </div>
      </div>
    </div>
    <div className="div-footer-img">
      <img className="footer-img" src="img/silhouette-crusade-crown-with-leaves-vector-11743761.jpg" />
    </div>
  </div>
);

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

const ERROR_CODE_ACCOUNT_EXISTS =
  'auth/account-exists-with-different-credential';

const ERROR_MSG_ACCOUNT_EXISTS = `
  An account with an E-Mail address to
  this social account already exists. Try to login from
  this account instead and associate your social accounts on
  your personal account page.
`;

class SignInFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { email, password } = this.state;

    this.props.firebase
      .doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { email, password, error } = this.state;

    const isInvalid = password === '' || email === '';

    return (

      <form onSubmit={this.onSubmit}>
        <div className="input_field"><span><i className="fa fa-envelope" aria-hidden="true" /></span>
          <input
            type="email"
            name="email"
            placeholder="Email"
            onChange={this.onChange}
            value={email} />
        </div>
        <div className="input_field"><span><i className="fa fa-lock" aria-hidden="true" /></span>
          <input
            type="password"
            name="password"
            placeholder="Password"
            onChange={this.onChange}
            value={password} />
        </div>
        <button disabled={isInvalid} id="button" type="submit" defaultValue="what in"
          style={{ borderRadius: '15px' }} >Sign in</button>
        {error && <p>{error.message}</p>
        }
      </form>
    );
  }
}

class SignInGoogleBase extends Component {
  constructor(props) {
    super(props);

    this.state = { error: null };
  }

  onSubmit = event => {
    this.props.firebase
      .doSignInWithGoogle()
      .then(socialAuthUser => {
        // Create a user in your Firebase Realtime Database too
        return this.props.firebase.user(socialAuthUser.user.uid).set({
          username: socialAuthUser.user.displayName,
          email: socialAuthUser.user.email,
          roles: {},
        });
      })
      .then(() => {
        this.setState({ error: null });
        this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        if (error.code === ERROR_CODE_ACCOUNT_EXISTS) {
          error.message = ERROR_MSG_ACCOUNT_EXISTS;
        }

        this.setState({ error });
      });

    event.preventDefault();
  };

  render() {
    const { error } = this.state;

    return (
      <div onClick={this.onSubmit} className="social_btn gplus"><a href="#">
        <span><i className="fa fa-google-plus" aria-hidden="true" ></i></span>Sign in with Google+</a>
        {error && <p>{error.message}</p>}
      </div>

      // <form onSubmit={this.onSubmit}>
      //   <button type="submit">Sign In with Google</button>

      //   {error && <p>{error.message}</p>}
      // </form>
    );
  }
}

class SignInFacebookBase extends Component {
  constructor(props) {
    super(props);

    this.state = { error: null };
  }

  onSubmit = event => {
    this.props.firebase
      .doSignInWithFacebook()
      .then(socialAuthUser => {
        // Create a user in your Firebase Realtime Database too
        return this.props.firebase.user(socialAuthUser.user.uid).set({
          username: socialAuthUser.additionalUserInfo.profile.name,
          email: socialAuthUser.additionalUserInfo.profile.email,
          roles: {},
        });
      })
      .then(() => {
        this.setState({ error: null });
        this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        if (error.code === ERROR_CODE_ACCOUNT_EXISTS) {
          error.message = ERROR_MSG_ACCOUNT_EXISTS;
        }

        this.setState({ error });
      });

    event.preventDefault();
  };

  render() {
    const { error } = this.state;

    return (
      <div onClick={this.onSubmit} className="social_btn fb"><a href="#"><span>
        <i className="fa fa-facebook" aria-hidden="true" /></span>Sign in with Facebook</a>
        {error && <p>{error.message}</p>}
      </div>


    );
  }
}

class SignInTwitterBase extends Component {
  constructor(props) {
    super(props);

    this.state = { error: null };
  }

  onSubmit = event => {
    this.props.firebase
      .doSignInWithTwitter()
      .then(socialAuthUser => {
        // Create a user in your Firebase Realtime Database too
        return this.props.firebase.user(socialAuthUser.user.uid).set({
          username: socialAuthUser.additionalUserInfo.profile.name,
          email: socialAuthUser.additionalUserInfo.profile.email,
          roles: {},
        });
      })
      .then(() => {
        this.setState({ error: null });
        this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        if (error.code === ERROR_CODE_ACCOUNT_EXISTS) {
          error.message = ERROR_MSG_ACCOUNT_EXISTS;
        }

        this.setState({ error });
      });

    event.preventDefault();
  };

  render() {
    const { error } = this.state;

    return (
      <div onSubmit={this.onSubmit} className="social_btn tw"><a href="#"><span><i className="fa fa-twitter" aria-hidden="true" />
      </span>Sign in with Twitter</a></div>

      // <form onSubmit={this.onSubmit}>
      //   <button type="submit">Sign In with Twitter</button>

      //   {error && <p>{error.message}</p>}
      // </form>
    );
  }
}

const SignInForm = compose(
  withRouter,
  withFirebase,
)(SignInFormBase);

const SignInGoogle = compose(
  withRouter,
  withFirebase,
)(SignInGoogleBase);

const SignInFacebook = compose(
  withRouter,
  withFirebase,
)(SignInFacebookBase);

const SignInTwitter = compose(
  withRouter,
  withFirebase,
)(SignInTwitterBase);

export default SignInPage;

export { SignInForm, SignInGoogle, SignInFacebook, SignInTwitter };
