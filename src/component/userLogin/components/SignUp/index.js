import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { withFirebase } from '../../../../Firebase';
import * as ROUTES from '../../../../constants/routes';
import "./signup.css";

const SignUpPage = () => (
  <div>
    <div>
      <div className="about_us">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="title_box">
                <h2>
                  Sign up
                </h2>
                <img className="img-fluid" src="img/title_line.png" />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="form_wrapper">
        <div className="form_container">
          <div className="row clearfix">
            <div className="col_half last">
              <SignUpForm />
            
            </div>
          </div>
        </div>
        <Link to={ROUTES.SIGN_IN}>
              <span className="fa-stack fa-3x"  aria-hidden="true">
                <i className="fa fa-arrow-left"></i>
              </span>
        </Link>
      </div>
      <div className="div-footer-img">

        <img className="footer-img" src="img/silhouette-crusade-crown-with-leaves-vector-11743761.jpg" />

      </div>



    </div>
  </div>
);

const INITIAL_STATE = {
  username: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  isAdmin: false,
  error: null,
};

const ERROR_CODE_ACCOUNT_EXISTS = 'auth/email-already-in-use';

const ERROR_MSG_ACCOUNT_EXISTS = `
    An account with this E-Mail address already exists.
    Try to login with this account instead. If you think the
    account is already used from one of the social logins, try
    to sign in with one of them. Afterward, associate your accounts
    on your personal account page.
  `;

class SignUpFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

    onSubmit = event => {
      const { username, email, passwordOne } = this.state;
      const roles = 'USER';

      this.props.firebase
        .doCreateUserWithEmailAndPassword(email, passwordOne)
        .then(authUser => {
          // Create a user in your Firebase realtime database
          return this.props.firebase.user(authUser.user.uid).set({
            username,
            email,
            roles,
          });
        })
        .then(() => {
          this.props.history.push(ROUTES.INFORMATION);
          return this.props.firebase.doSendEmailVerification();
        })
        .then(() => {
          this.setState({ ...INITIAL_STATE });

        })
        .catch(error => {
          if (error.code === ERROR_CODE_ACCOUNT_EXISTS) {
            error.message = ERROR_MSG_ACCOUNT_EXISTS;
          }

          this.setState({ error });
        });

      event.preventDefault();
    };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onChangeCheckbox = event => {
    this.setState({ [event.target.name]: event.target.checked });
  };

  render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      error,
    } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      email === '' ||
      username === '';

    return (
      <form onSubmit={this.onSubmit}>
        <div className="input_field"><span><i className="fa fa-user" aria-hidden="true" /></span>
          <input
            name="username"
            value={username}
            onChange={this.onChange}
            type="text"
            placeholder="Full Name"
          />
        </div>
        <div className="input_field"><span><i className="fa fa-envelope" aria-hidden="true" /></span>
          <input
            name="email"
            value={email}
            onChange={this.onChange}
            type="email"
            placeholder="Email Address"
          />
        </div>
        <div className="input_field"><span><i className="fa fa-lock" aria-hidden="true" /></span>
          <input
            name="passwordOne"
            value={passwordOne}
            onChange={this.onChange}
            type="password"
            placeholder="Password"
          />
        </div>
        <div className="input_field"><span><i className="fa fa-lock" aria-hidden="true" /></span>
          <input
            name="passwordTwo"
            value={passwordTwo}
            onChange={this.onChange}
            type="password"
            placeholder="Confirm Password"
          />
        </div>

        <button disabled={isInvalid} id="button" type="submit" defaultValue="what in"
          style={{ borderRadius: '15px' }} >Sign up</button>
        {error && <p>{error.message}</p>
        }
      </form>
    );
  }
}


const SignUpForm = compose(
  withRouter,
  withFirebase,
)(SignUpFormBase);

export default SignUpPage;

export { SignUpForm };
