import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { withFirebase } from '../../../../Firebase';
import * as ROUTES from '../../../../constants/routes';

const PasswordForgetPage = () => (
  <div>
    <div className="about_us">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="title_box">
              <h2>ReSet your password</h2>
              <img className="img-fluid" src="img/title_line.png" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="form_wrapper">
      <div className="form_container">
        <div className="row clearfix">
          <div className="col_half last">
            <PasswordForgetForm></PasswordForgetForm>
          </div>
        </div>
      </div>
      <Link to={ROUTES.SIGN_IN}>
              <span className="fa-stack fa-3x"  aria-hidden="true">
                <i className="fa fa-arrow-left"></i>
              </span>
        </Link>
    </div>
    
  </div>
);

const INITIAL_STATE = {
  email: '',
  error: null,
};

class PasswordForgetFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { email } = this.state;

    this.props.firebase
      .doPasswordReset(email)
      .then(() => {
        this.props.history.push(ROUTES.INFORMATION)
        this.setState({ ...INITIAL_STATE });
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { email, error } = this.state;

    const isInvalid = email === '';

    return (
      <form onSubmit={this.onSubmit}>
        <div className="input_field"><span><i className="fa fa-envelope" aria-hidden="true" /></span>
          <input
            name="email"
            value={this.state.email}
            onChange={this.onChange}
            type="text"
            placeholder="Email Address"
          />
        </div>
        <button disabled={isInvalid} id="button" type="submit" defaultValue="what in"
          style={{ borderRadius: '15px' }} >ReSet</button>
        {error && <p>{error.message}</p>
        }
      </form>
    );
  }
}

// const PasswordForgetLink = () => (

//     <Link className="col_half forgot_pw" to={ROUTES.PASSWORD_FORGET}> <a href="#">Forgot Password?</a> </Link>

// );

export default PasswordForgetPage;

const PasswordForgetForm = withFirebase(PasswordForgetFormBase);

export { PasswordForgetForm };
