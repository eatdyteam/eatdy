import React, { Component } from 'react';
import HeightRow from './HeightRow';
import { Link } from 'react-router-dom';
import * as ROUTES from '../../../../constants/routes';
import {  Redirect } from 'react-router-dom';  
import { withFirebase } from '../../../../Firebase';
import {
    AuthUserContext,
} from '../Session';

class InformationUser extends Component {

    constructor(props) {
        super(props);
        this.state = {

            firstName: "",
            lastName: "",
            phone: "",
            address: "",
            city: "",
            weight: "",
            height: "",
            wish: "",
            chedo: "BinhThuong",
            menu:[],
            menus:[]

        }
    }

    componentWillMount() {
        this.getData();
    }

    getData=()=>{
     var menus = [];

     this.props.firebase.food().on('value', (list) => {

         list.forEach(element => {
             menus.push(element.val());
             // }
         });
         this.setState(
             {
                 menu: menus
             })
     });
 }






    isChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState(
            { [name]: value }
        );

    }



    onSubmit = (event,id, name, role, email1) => {
        event.preventDefault();
        var menus =[];
        this.state.menu.map((value,key) =>{

                if(value.cheDo==this.state.chedo){
                    menus.push(value);
                }

            }
        )
        this.setState({
            menus:menus
        });



        const email = email1;
        const roles = role;
        const username = name;
        const {
            firstName,
            lastName,
            phone,
            address,
            city,
            weight,
            height,
            wish,
            chedo
           
        } = this.state;

        this.props.firebase.user(id)
            .set({
                email,
                roles,
                username,
                firstName,
                lastName,
                phone,
                address,
                city,
                weight,
                height,
                wish,
                chedo,
                menus
            }).then(success => {
                console.log(success);
                window.location.href = '/account';
            });

    }




    getHeightRow = () => {
        for (var i = 1; i <= 100; i++) {
            this.HeightRow(i);
        }
    }

    HeightRow = (value) => {
        return <HeightRow value={value}></HeightRow>
    }

    render() {
            console.log(this.state.menus);
        return (
            <div>
                <AuthUserContext.Consumer>
                    {authUser => (
                        <div>
                            <div className="about_us">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="title_box">
                                                <h2>Your information</h2>
                                                <img className="img-fluid" src="img/title_line.png" />
                                                <div id="chamngon">
                                                    <p>
                                                        Please tell us about you. We will help you to get the health you always wanted!
            </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="container">
                                <form className="well form-horizontal" id="contact_form">
                                    <fieldset>
                                        {/* Form Name */}
                                        {/* Text input*/}
                                        <div className="form-group">
                                            <label className="col-md-4 control-label">First Name </label>
                                            <div className="col-md-4 inputGroupContainer">
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user" /></span>
                                                    <input name="firstName" placeholder="First Name"
                                                        className="form-control"
                                                        type="text"
                                                        onChange={(event) => this.isChange(event)} />

                                                </div>
                                            </div>
                                        </div>
                                        {/* Text input*/}
                                        <div className="form-group">
                                            <label className="col-md-4 control-label">Last Name</label>
                                            <div className="col-md-4 inputGroupContainer">
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="glyphicon glyphicon-user" /></span>
                                                    <input name="lastName" placeholder="Last Name"
                                                        className="form-control" type="text"
                                                        onChange={(event) => this.isChange(event)} />
                                                </div>
                                            </div>
                                        </div>
                                        {/* Text input*/}
                                        {/* <div class="form-group">
                              <label class="col-md-4 control-label">E-Mail</label>
                              <div class="col-md-4 inputGroupContainer">
                                  <div class="input-group">
                                      <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                      <input name="email" placeholder="E-Mail Address" class="form-control" type="text">
                                  </div>
                              </div>
                          </div> */}
                                        {/* Text input*/}
                                        <div className="form-group">
                                            <label className="col-md-4 control-label">Phone</label>
                                            <div className="col-md-4 inputGroupContainer">
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="glyphicon glyphicon-earphone" /></span>
                                                    <input name="phone" placeholder="(845)555-1212"
                                                        className="form-control" type="text"
                                                        onChange={(event) => this.isChange(event)} />
                                                </div>
                                            </div>
                                        </div>
                                        {/* Text input*/}
                                        <div className="form-group">
                                            <label className="col-md-4 control-label">Address</label>
                                            <div className="col-md-4 inputGroupContainer">
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="glyphicon glyphicon-home" /></span>
                                                    <input name="address" placeholder="Address"
                                                        className="form-control" type="text"
                                                        onChange={(event) => this.isChange(event)} />
                                                </div>
                                            </div>
                                        </div>
                                        {/* Text input*/}
                                        <div className="form-group">
                                            <label className="col-md-4 control-label">City</label>
                                            <div className="col-md-4 inputGroupContainer">
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="fa fa-home" aria-hidden="true" /></span>
                                                    <input name="city" placeholder="city" className="form-control" type="text"
                                                        onChange={(event) => this.isChange(event)} />
                                                </div>
                                            </div>
                                        </div>
                                        {/* Select Basic */}
                                        <div className="form-group">
                                            <label className="col-md-4 control-label">Weight(*kg)</label>
                                            <div className="col-md-4 selectContainer">
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="fa fa-male" /></span>
                                                    <select name="weight" className="form-control selectpicker"
                                                        style={{ height: '32px',margin:'0' }}

                                                        onChange={(event) => this.isChange(event)}>
                                                        <option value=" ">Please select your weight</option>
                                                        <option value=" ">10</option>
                                                        <option value=" ">11</option>
                                                        <option value=" ">12</option>
                                                        <option value=" ">13</option>
                                                        <option value=" ">14</option>
                                                        <option value=" ">15</option>
                                                        <option value=" ">16</option>
                                                        <option value=" ">17</option>
                                                        <option value=" ">18</option>
                                                        <option value=" ">19</option>
                                                        <option value=" ">20</option>
                                                        <option value=" ">21</option>
                                                        <option value=" ">22</option>
                                                        <option value=" ">23</option>
                                                        <option value=" ">24</option>
                                                        <option value=" ">25</option>
                                                        <option value=" ">26</option>
                                                        <option value=" ">27</option>
                                                        <option value=" ">28</option>
                                                        <option value=" ">29</option>
                                                        <option value=" ">30</option>
                                                        <option value=" ">31</option>
                                                        <option value=" ">32</option>
                                                        <option value=" ">33</option>
                                                        <option value=" ">34</option>
                                                        <option value=" ">35</option>
                                                        <option value=" ">36</option>
                                                        <option value=" ">37</option>
                                                        <option value=" ">38</option>
                                                        <option value=" ">39</option>
                                                        <option value=" ">40</option>
                                                        <option value=" ">41</option>
                                                        <option value=" ">42</option>
                                                        <option value=" ">43</option>

                                                        
                                                        <option value=" ">44</option>
                                                        <option value=" ">45</option>
                                                        <option value=" ">46</option>
                                                        <option value=" ">47</option>
                                                        <option value=" ">48</option>
                                                        <option value=" ">49</option>
                                                        <option value=" ">50</option>
                                                        <option value=" ">51</option>
                                                        <option value=" ">52</option>
                                                        <option value=" ">53</option>
                                                        <option value=" ">54</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="col-md-4 control-label">Height(*cm)</label>
                                            <div className="col-md-4 selectContainer">
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="fa fa-male" /></span>
                                                    <select name="height" className="form-control selectpicker"
                                                        onChange={(event) => this.isChange(event)} style={{ height: '32px' ,margin:'0' }}>
                                                        <option value=" ">Please select your Height</option>
                                                        <option value=" ">110</option>
                                                        <option value=" ">111</option>
                                                        <option value=" ">112</option>
                                                        <option value=" ">113</option>
                                                        <option value=" ">114</option>
                                                        <option value=" ">115</option>
                                                        <option value=" ">116</option>
                                                        <option value=" ">117</option>
                                                        <option value=" ">118</option>
                                                        <option value=" ">119</option>
                                                        <option value=" ">120</option>
                                                        <option value=" ">121</option>
                                                        <option value=" ">122</option>
                                                        <option value=" ">123</option>
                                                        <option value=" ">124</option>
                                                        <option value=" ">125</option>
                                                        <option value=" ">126</option>
                                                        <option value=" ">127</option>
                                                        <option value=" ">128</option>
                                                        <option value=" ">129</option>
                                                        <option value=" ">130</option>
                                                        <option value=" ">131</option>
                                                        <option value=" ">132</option>
                                                        <option value=" ">133</option>
                                                        <option value=" ">134</option>
                                                        <option value=" ">135</option>
                                                        <option value=" ">136</option>
                                                        <option value=" ">137</option>
                                                        <option value=" ">138</option>
                                                        <option value=" ">139</option>
                                                        <option value=" ">140</option>
                                                        <option value=" ">141</option>
                                                        <option value=" ">142</option>
                                                        <option value=" ">143</option>
                                                        <option value=" ">144</option>
                                                        <option value=" ">145</option>
                                                        <option value=" ">146</option>
                                                        <option value=" ">147</option>
                                                        <option value=" ">148</option>
                                                        <option value=" ">149</option>
                                                        <option value=" ">150</option>
                                                        <option value=" ">151</option>
                                                        <option value=" ">152</option>
                                                        <option value=" ">153</option>
                                                        <option value=" ">154</option>

                                                        

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="col-md-4 control-label">Choice(*  )</label>
                                            <div className="col-md-4 selectContainer">
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="fa fa-male" /></span>
                                                    <select name="chedo" className="form-control selectpicker" style={{ height: '32px',margin:'0'  }} onChange={(event) => this.isChange(event)}>
                                                        <option value="BinhThuong">Bình thường</option>
                                                        <option value="GiamCan">Giảm cân</option>
                                                        <option value="TangCan">Tăng cân</option>
                                                        <option value="TapGym">Tập gym</option>
                                                        <option value="TapYoga">Tập Yoga</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        {/* Text input*/}
                                        {/* Select Basic */}

                                        {/* Text input*/}

                                        {/* Text input*/}

                                        {/* radio checks */}
                                        <div className="form-group">
                                            <label className="col-md-4 control-label">your gender</label>
                                            <div className="col-md-4">
                                                <div className="radio">
                                                    <label>
                                                        <input type="radio" name="hosting" defaultValue="yes" /> Male
                      </label>
                                                </div>
                                                <div className="radio">
                                                    <label>
                                                        <input type="radio" name="hosting" defaultValue="no" /> Female
                      </label>
                                                </div>
                                            </div>
                                        </div>
                                        {/* Text area */}
                                        <div className="form-group">
                                            <label className="col-md-4 control-label">Wish</label>
                                            <div className="col-md-4 inputGroupContainer">
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="glyphicon glyphicon-pencil" /></span>
                                                    <textarea className="form-control" name="wish"
                                                        placeholder="Your wishes" defaultValue={""}
                                                        onChange={(event) => this.isChange(event)} />
                                                </div>
                                            </div>
                                        </div>
                                        {/* Success message */}
                                        <div className="alert alert-success" role="alert" id="success_message">Success <i className="glyphicon glyphicon-thumbs-up" /> Thanks for contacting us, we will get back to you
                  shortly.</div>
                                        {/* Button */}
                                        <div className="form-group">
                                            <label className="col-md-4 control-label" />
                                            <div className="col-md-4">
                                                {/* <Link to={ROUTES.ACCOUNT} onClick={() => this.onSubmit(authUser.uid, authUser.username, authUser.roles, authUser.email)}> <button type="submit" className="btn btn-warning">Send <span className="glyphicon glyphicon-send" /></button></Link> */}
                                                <button className="btn btn-warning" onClick={(event) => this.onSubmit(event,authUser.uid, authUser.username, authUser.roles, authUser.email)}>Send</button>
                                            </div>

                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>

                    )}
                </AuthUserContext.Consumer>
            </div >

        );
    }
}

export default withFirebase(InformationUser);