import React, { Component } from 'react';
import Title from '../Title';

class NewFocus extends Component {
    render() {
        return (
            <div className="about_us">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                           <Title title="News Focus"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="about_con">
                            <h3>Because of high profits, many people are willing to fight back against the authorities to bring dirty food to the domestic market.</h3>
                            <p>Recently, when the Hanoi city authorities discovered a truck carrying dirty organs, the engine continued to run away at a speed of a hundred km / h. Finally, after the nearly 10km track, this car only stopped when it hit the traffic police car directly, causing the car's tail to be distorted and deformed.</p>
                            <p>Through the inspection of the functional forces, on the car loaded with pig's broth, it is estimated that the weight is about 3 tons. According to the labels on each block of meat, this number of products originated from across the border, not quarantined by any agency, allowed to import. Despite being frozen, but the number of piggles has been damaged because every piece of meat appears black and blue mold.</p>
                            <div className="about_btn"> <a href="http://dienbientv.vn/tin-tuc-su-kien/xa-hoi/201901/cam-go-cuoc-chien-ngan-chan-viec-nhap-lau-thuc-pham-ban-cuoi-nam-5615255/" className="aboutbtn">Read More</a> </div>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="about_img"> <img className="img-fluid" src="img/new-ban.jpg" /> </div>
                            <div className="about_img1"> <img className="img-fluid" src="img/new-ban1.png" />
                            <div className="about_img_2"> <img className="img-fluid" src="img/new-ban2.jpg" />
                            </div>
                            </div>
                        </div>
                    </div>
                </div>           
            </div>
        );
    }
}

export default NewFocus;