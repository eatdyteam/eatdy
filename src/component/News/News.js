import React, { Component } from 'react';
import WhatWeDo from '../AboutUs/WhatWeDo';
import Detail from './Detail';
import HeaderSection from '../header/HeaderSection/HeaderSection';
import NewFocus from './NewFocus';

class News extends Component {
    render() {
        return (
            <div>
                <HeaderSection nameHD="News" nameHT="News"/>
                <NewFocus/>
                <Detail/>
            </div>
        );
    }
}

export default News;