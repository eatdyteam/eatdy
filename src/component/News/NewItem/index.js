import React, { Component } from 'react';

class NewItem extends Component {
    render() {
        return (
            <div className="owl-item cloned" style={{width: '270px'}}>
                <div className="item">
                    <div className="item1">
                        <div className="team-img"> <img src={this.props.imgItem} alt="coffer" className="img-fluid" />
                            <div className="team-bottom-box">
                                <div className="bottom-team-border">
                                <h2>{this.props.nameItem}</h2>
                                <p>{this.props.textItem}</p>
                                <h4><a href={this.props.linkItem}>Read more</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default NewItem;