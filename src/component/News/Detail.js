import React, { Component } from 'react';
import Title from '../Title'
import NewItem from './NewItem';


class Detail extends Component {
    render() {
        return (
            <div className="instructors">
                <div className="container">
                    <div className="row">
                        <Title title="Related News"/>
                        <div className="col-lg-12">
                            <div className="owl-carousel owl-theme ss_carousel owl-loaded owl-drag" id="carousel">
                                <div className="owl-stage-outer">
                                    <div className="owl-stage" style={{transform: 'translate3d(-1080px, 0px, 0px)', transition: 'all 0s ease 0s', width: '3510px'}}>
                                        <NewItem nameItem="News" textItem="Vi - Cumax Nano Curcumin products show signs of deceiving consumers." linkItem="https://vnreview.vn/headlines-detail/-/headline/can-trong-voi-th-244-ng-tin-quang-c-225-o-thuc-pham-bao-ve-suc-khoe-tr-224-slim-cuong-anh-tr-234-n-mot-so-website" imgItem="img/new-detail1.png"/>
                                        <NewItem nameItem="News" textItem="V10 interesting things about the digestive system in humans." linkItem="https://hellobacsi.com/ban-tin-suc-khoe/he-tieu-hoa-o-nguoi/" imgItem="img/new-detail2.png"/>
                                        <NewItem nameItem="News" textItem="[Infographic] 8 great way to have a good shape." linkItem="https://hellobacsi.com/ban-tin-suc-khoe/infographic-8-tuyet-chieu-de-co-mot-voc-dang-can-doi/" imgItem="img/new-detail3.jpg"/>
                                        <NewItem nameItem="News" textItem="Advanced methods for screening 4 common types of cancer in women." linkItem="https://hellobacsi.com/ban-tin-suc-khoe/tam-soat-ung-thu-thuong-gap-o-phu-nu/" imgItem="img/new-detail4.png"/>
                                        <NewItem nameItem="News" textItem="Tips to help dark skin suddenly turn white and smooth." linkItem="https://hellobacsi.com/ban-tin-suc-khoe/bi-kip-giup-da-ngam-den-bong-trang-bat-tong-va-min-mang/" imgItem="img/new-detail5.jpg"/>
                                        <NewItem nameItem="News" textItem="Vi - Cumax Nano Curcumin products show signs of deceiving consumers." linkItem="https://vnreview.vn/headlines-detail/-/headline/can-trong-voi-th-244-ng-tin-quang-c-225-o-thuc-pham-bao-ve-suc-khoe-tr-224-slim-cuong-anh-tr-234-n-mot-so-website" imgItem="img/new-detail1.png"/>
                                        <NewItem nameItem="News" textItem="V10 interesting things about the digestive system in humans." linkItem="https://hellobacsi.com/ban-tin-suc-khoe/he-tieu-hoa-o-nguoi/" imgItem="img/new-detail2.png"/>
                                        <NewItem nameItem="News" textItem="[Infographic] 8 great way to have a good shape." linkItem="https://hellobacsi.com/ban-tin-suc-khoe/infographic-8-tuyet-chieu-de-co-mot-voc-dang-can-doi/" imgItem="img/new-detail3.jpg"/>
                                        <NewItem nameItem="News" textItem="Advanced methods for screening 4 common types of cancer in women." linkItem="https://hellobacsi.com/ban-tin-suc-khoe/tam-soat-ung-thu-thuong-gap-o-phu-nu/" imgItem="img/new-detail4.png"/>
                                        <NewItem nameItem="News" textItem="Tips to help dark skin suddenly turn white and smooth." linkItem="https://hellobacsi.com/ban-tin-suc-khoe/bi-kip-giup-da-ngam-den-bong-trang-bat-tong-va-min-mang/" imgItem="img/new-detail5.jpg"/>
                 
                                       
                                    </div>
                                </div>
                                    <div className="owl-nav disabled">
                                        <div className="owl-prev">
                                            <i className="fa fa-angle-left" />
                                        </div>
                                        <div className="owl-next">
                                            <i className="fa fa-angle-right" />
                                        </div>
                                    </div>
                                        <div className="owl-dots">
                                            <div className="owl-dot active">
                                                <span />
                                            </div>
                                            <div className="owl-dot">
                                                <span />
                                            </div>
                                            
                                        </div>
                                    </div>

                                </div> 
                            </div>
                        </div>
                    </div>

                    );
                }
            }

export default Detail;