import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import * as ROUTES from "../../constants/routes"
// import Home from '../component/Home';
import SignUpPage from '../userLogin/components/SignUp';
import SignInPage from '../userLogin/components/SignIn';
import InformationUser from '../userLogin/components/InformationUser'
import AboutUs from '../AboutUs/AboutUs';
import News from '../News/News';
import Food from '../Food/Food';
import Health from '../Health/Health';
import Contacts from '../Contact/Contacts';
import Home from '../Home/Home';
import AccountPage from '../userLogin/components/Account';
import Test from '../userLogin/components/TestFirebase';
import  PasswordForgetPage  from '../userLogin/components/PasswordForget';
import Menu from '../../database/Menu';
import Map from '../GoogleMap';


class Routers extends Component {
    render() {
        return (
            
                <div>
                    <Switch>
                    <Route exact path={ROUTES.HOME} component={Home}/>
                    <Route path={ROUTES.ABOUT} component={AboutUs}/>
                    <Route path={ROUTES.NEW} component={News}/>
                    <Route path={ROUTES.FOOD} component={Food}/>
                    <Route path={ROUTES.HEALTH} component={Health}/>
                    <Route path={ROUTES.CONTACT} component={Contacts}/>
                    <Route path={ROUTES.SIGN_IN} component={SignInPage} />
                    <Route path={ROUTES.SIGN_UP} component={SignUpPage} />
                    <Route path={ROUTES.INFORMATION} component={InformationUser} />
                    <Route path={ROUTES.ACCOUNT} component={AccountPage}/>
                    <Route path={ROUTES.PASSWORD_FORGET} component={PasswordForgetPage}/>
                    <Route path={ROUTES.MENU} component={Menu}/>
                    <Route path={ROUTES.MAP} component={Map}/>
                    <Route path="/test" component={Test}/>
                    </Switch>
                </div>
           
        );
    }
}

export default Routers;
