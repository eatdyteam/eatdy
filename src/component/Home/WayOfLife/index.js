import React, { Component } from 'react';
import WayOfLifeItem from './WayOfLifeItem';
import Title from '../../Title';


class WayOfLife extends Component {
    render() {
        return (
            
            <div className="yoga_bg">
            <div className="container">
                <Title title="Eatdy as a way of life"/>
                <div className="row">
                <div className="col-md-12">
                    <div className="first_col">
                    <WayOfLifeItem nameWayOf="People gaining weight" textWayOf=" Diet is suitable to have the desired body shape but still ensures health. " icon="flaticon-yoga-1"/>
                    <WayOfLifeItem nameWayOf="Gym people" textWayOf=" Diet is suitable to have the desired body shape but still ensures health." icon="flaticon-yoga-5"/>
                    </div>
                    <div className="first_col2">
                    <div className="yoga_img_box"> <img className="img-fluid" src="img/about2-removebg-preview.png" /> </div>
                    </div>
                    <div className="first_col3">
                    <WayOfLifeItem nameWayOf="People lose weight" textWayOf=" Diet is suitable to have the desired body shape but still ensures health." icon="flaticon-yoga-4"/>
                    <WayOfLifeItem nameWayOf="People practicing yoga" textWayOf=" Diet is suitable to have the desired body shape but still ensures health." icon="flaticon-yoga-3"/>
                    </div>
                </div>
                </div>
            </div>
            </div>

        );
    }
}

export default WayOfLife;