import React, { Component } from 'react';

class WayOfLifeItem extends Component {
    render() {
        return (
            <div className="yoga_box">
                <div className="yoga_con">
                    <h3>{this.props.nameWayOf}</h3>
                    <p> {this.props.textWayOf} </p>
                </div>
                <div className="yoga_img"> <div className="icon_box"><i className={this.props.icon} /></div></div>
            </div>

        );
    }
}

export default WayOfLifeItem;