import React, { Component } from 'react';
import About from './About'
import WayOfLife from './WayOfLife'
import HealthNew from './HeathNew';
import Demo from './Demo';
import Foods from './Foods';
import HeaderSection from './HeaderSection';

class Home extends Component {
    render() {
        return (
            <div>
                <HeaderSection/>
                <About/>
                <WayOfLife/>
                <HealthNew/>
                <Demo/>
                <Foods/>
            </div>
        );
    }
}

export default Home;