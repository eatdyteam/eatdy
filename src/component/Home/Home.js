import React, { Component } from 'react';
import AboutEatdy from '../AboutUs/AboutEatdy';
import WayOfLife from './WayOfLife';
import HealthNew from '../Health';
import Demo from '../AboutUs/Demo/Demo';
import FoodDetail from '../Food/FoodDetail/FoodDetail';
import HeaderSectionHome from '../HeaderSectionHome';

class Home extends Component {
    render() {
        return (
            <div>
                <HeaderSectionHome/>
                <AboutEatdy/>
                <WayOfLife/>
                <HealthNew/>
                <Demo/>
                <FoodDetail/>
            </div>
        );
    }
}

export default Home;