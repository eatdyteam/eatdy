import React, { Component } from 'react';
import * as ROUTES from "../../constants/routes";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

class Footer extends Component {
    render() {
        return (
            <div>
                {/* start footer */}
                <footer>
                    <div className="container">
                    <div className="row">
                        <div className="col-lg-2">
                        <div className="footer_con">
                            <h2 className="nav-brand footer_link"><a href="index.html" style={{fontSize: '56px', color: 'white', paddingBottom: '31px', fontFamily: 'fantasy', fontWeight: 'lighter'}} className="logo">eatdy</a></h2>
                            <div className="footer_social">
                            <ul>
                                <li><a href="#"><i className="fa fa-twitter" aria-hidden="true" /></a></li>
                                <li><a href="#"><i className="fa fa-facebook" aria-hidden="true" /></a></li>
                                <li><a href="#"><i className="fa fa-google-plus" aria-hidden="true" /></a></li>
                            </ul>
                            </div>
                        </div>
                        </div>
                        <div className="col-lg-7">
                        <div className="footer_center_con">
                            <ul className="footer_link">
                                <li><Link to={ROUTES.HOME}>Home</Link></li>
                                <li><Link to={ROUTES.ABOUT}>About Us</Link></li>
                                <li><Link to={ROUTES.NEW}>News</Link></li>
                                <li><Link to={ROUTES.FOOD}>Food</Link></li>
                                <li><Link to={ROUTES.HEALTH}>Health</Link></li>
                                <li><Link to={ROUTES.CONTACT}>Contact</Link></li>
                            </ul>
                            <div className="footer_border" />
                            <p> “To insure good health: eat lightly, breathe deeply, live moderately, cultivate cheerfulness, and maintain an interest in life.” </p>
                            <p style={{float: 'right', fontSize: '10px', fontFamily: 'Cambria, Cochin, Georgia, Times, "Times New Roman", serif'}}>William Londen</p>
                        </div>
                        </div>
                        <div className="col-lg-3">
                        <div className="news_letter_box">
                            <h3>Subscribe for yourself</h3>
                            <div className="card-box-input">
                            <div className="slider_btn" style={{textAlign: 'center', fontSize: '12px', paddingBottom: '18px', paddingRight: '20px'}}><a href="#" className="sliderbtn"> Registered </a> </div>
                            </div>
                            <p>to get advice on healthy diets</p>
                        </div>
                        </div>
                    </div>
                    </div>
                </footer>
                {/* end footer*/}
                {/* start footer_bottom */}
                <div className="footer_bootom">
                    <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                        <div className="copy_right">
                            <p>© 2019 EATDY.</p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                {/* end footer_bottom */}
            </div>
        );
    }
}

export default Footer;