import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Navigation from './Navigation';
import { withAuthentication } from './component/userLogin/components/Session';
import Footer from './component/footer';
import Routers from './component/router';
import Food from './component/Food/Food';


class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navigation/>
            <Routers/>
            <Footer/>   
        </div>
      </Router>
    );
  }
}

export default withAuthentication(App);
