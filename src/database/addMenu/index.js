import React, { Component } from 'react';




class AddMenu extends Component {
  constructor(props) {
    super(props);
    this.state={
      cheDo:'BinhThuong',
      buoi:'SANG',
      cachlam:'',
      cachlam1:'',
      cachlam2:'',
      ten:'',
      ten1:'',
      ten2:'',
      hinhanh:'',
      hinhanh1:'',
      hinhanh2:''
    }
  }
  
  isChange  = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState(
        {[name] : value}
    );
}


chonCheDo =(event) =>{
      this.setState({
        cheDo:event.target.value
      });
    }
chonBuoi =(event) =>{
      this.setState({
        buoi:event.target.value
      });
    }

  pushReset=()=>{
    this.props.pushData(this.state.cheDo,this.state.buoi,this.state.ten,this.state.cachlam,this.state.hinhanh,this.state.ten1,this.state.cachlam1,this.state.hinhanh1,this.state.ten2,this.state.cachlam2,this.state.hinhanh2);
    this.setState({
      cheDo:'BinhThuong',
      buoi:'SANG',
      cachlam:'',
      cachlam1:'',
      cachlam2:'',
      ten:'',
      ten1:'',
      ten2:'',
      hinhanh:'',
      hinhanh1:'',
      hinhanh2:''
    });
  }

  render() {
   
    return (
      <div className="container mt-5 " >
          <div className='col-12'>
        <select value={this.state.cheDo}  className='d-inline-block  m-5 ' onChange={(event)=>this.chonCheDo(event)}>
            <option value="BinhThuong">Bình Thường</option>
            <option value="GiamCan">Giảm Cân</option>
            <option value="TangCan">Tăng Cân</option>
            <option value="TapGym">Tập Gym</option>
            <option value="TapYoga">Tập Yoga</option>
        </select>
        <select className='d-inline-block p-2 mb-3' value={this.state.buoi}  onChange={(event)=>this.chonBuoi(event)}>
            <option value="SANG">Sáng</option>
            <option value="TRUA">Trưa</option>
            <option value="TOI">Tối</option>
        </select>
          </div>
       
        <div className='d-inline-block col-4 mon form' >
             <p className='mt-5 font-weight-bold'>MÓN 1</p>
            <input className='d-block p-2 mb-3' value={this.state.ten} placeholder="Tên" name="ten" onChange={(event)=>this.isChange(event)} />
            <input className='d-block p-2 mb-3' value={this.state.cachlam}  placeholder="Cách Làm" name="cachlam" onChange={(event)=>this.isChange(event)} />
            <input className='d-block p-2 mb-3' value={this.state.hinhanh}  placeholder="Hình ảnh" name="hinhanh" onChange={(event)=>this.isChange(event)}  />
        </div>
        <div className='d-inline-block col-4 mon form'>
            <p className='mt-5 font-weight-bold'>MÓN 2</p>
            <input className='d-block p-2 mb-3' value={this.state.ten1}  placeholder="Tên" name="ten1"  onChange={(event)=>this.isChange(event)}/>
            <input className='d-block p-2 mb-3' value={this.state.cachlam1}  placeholder="Cách làm" name="cachlam1"  onChange={(event)=>this.isChange(event)}/>
            <input className='d-block p-2 mb-3' value={this.state.hinhanh1}  placeholder="Hình ảnh" name="hinhanh1"  onChange={(event)=>this.isChange(event)}/>     
        </div>
        <div className='d-inline-block col-4 mon form'>
            <p className='mt-5 font-weight-bold'>MÓN 3</p>
            <input className='d-block p-2 mb-3' value={this.state.ten2}  placeholder="Tên" name="ten2"  onChange={(event)=>this.isChange(event)}/>
            <input className='d-block p-2 mb-3' value={this.state.cachlam2}  placeholder="Cách làm" name="cachlam2"  onChange={(event)=>this.isChange(event)}/>
            <input className='d-block p-2 mb-3' value={this.state.hinhanh2}  placeholder="Hình ảnh" name="hinhanh2"  onChange={(event)=>this.isChange(event)}/>

        </div>
               <button className='aboutbtn ml-4' style={{width:'120px'}} onClick={()=>this.pushReset()}>ok</button>
      </div>
    );
  }
}

export default AddMenu;
// onClick={(cheDo,buoi,ten,cachlam,hinhanh,ten1,cachlam1,hinhanh1,ten2,cachlam2,hinhanh2)=>this.props.pushData(this.state.cheDo,this.state.buoi,this.state.ten,this.state.cachlam,this.state.hinhanh,this.state.ten1,this.state.cachlam1,this.state.hinhanh1,this.state.ten2,this.state.cachlam2,this.state.hinhanh2)}