import React, { Component } from 'react';
import { withFirebase } from '../../Firebase';
import Row from './tableRow';
import AddMenu from '../addMenu';
import  './Menu.css';
import {
    AuthUserContext,
    withAuthorization,
    withEmailVerification,
  } from '../../component/userLogin/components/Session';
import { auth } from 'firebase';



class ShowMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
           show :'BinhThuong',
            menu: [],
            key: [],

        }

    }
    componentWillMount() {
        this.getDataFromDB();
    }

    getDataFromDB() {
        var menu = [];
        this.props.firebase.food().on('value', (list) => {

            list.forEach(element => {
                menu.push(element.val());

            });
            this.setState(
                {
                    menu: menu
                })
        });

    }

    pushData = (cheDo, buoi, ten, cachlam, hinhanh, ten1, cachlam1, hinhanh1, ten2, cachlam2, hinhanh2) => {
        var item = {};
        item = {
            cheDo: cheDo,
            buoi: buoi,
            m1: {
                ten: ten,
                cachlam: cachlam,
                hinhanh: hinhanh
            },
            m2: {
                ten: ten1,
                cachlam: cachlam1,
                hinhanh: hinhanh1
            },
            m3: {
                ten: ten2,
                cachlam: cachlam2,
                hinhanh: hinhanh2
            }
        };
        this.props.firebase.food().push(item);
        this.getDataFromDB();   
    }

    showMenu=()=>{
    
       return <table className='ml-5' id='menu'>
        <thead>
            <tr>       
                <th>id thực đơn</th>
                <th>Tên</th>
                <th>Buổi</th>
                <th>Hình Ảnh</th>
                <th>Cách Làm</th>

            </tr>
        </thead>
        
        {this.state.menu.map((value, key) => {
           
        if(value.cheDo==this.state.show){
            return<Row id={key}
            ten={value.m1.ten}
            cachlam={value.m1.cachlam}
            hinhanh={value.m1.hinhanh}
            buoi={value.buoi}
            ten2={value.m2.ten}
            cachlam2={value.m2.cachlam}
            hinhanh2={value.m2.hinhanh}
            ten3={value.m3.ten}
            cachlam3={value.m3.cachlam}
            hinhanh3={value.m3.hinhanh}
            
        />          
        }
    })}
    
   
   </table>
    }
    showTable=(cheDo)=>{
        this.setState({
            show:cheDo
        });
    }

    render() {

        return (
            <AuthUserContext.Consumer >
                 {authUser => (
            <div className='container menu' >
                <AddMenu pushData={this.pushData} />
                <div className="link_box mt-5">
                    <button className="btn btn-default filter-button"  onClick={(value)=>{this.showTable('BinhThuong')}} data-filter="BinhThuong">Bình thường</button>
                    <button className="btn btn-default filter-button" onClick={(value)=>{this.showTable('GiamCan')}} data-filter="GiamCan">Giảm cân</button>
                    <button className="btn btn-default filter-button" onClick={(value)=>{this.showTable('TangCan')}} data-filter="TangCan">Tăng cân</button>
                    <button className="btn btn-default filter-button " onClick={(value)=>{this.showTable('TapGym')}} data-filter="TapGym">Gym</button>
                    <button className="btn btn-default filter-button" onClick={(value)=>{this.showTable('TapYoga')}} data-filter="TapYoga">Yoga</button>
                </div>

                     {this.showMenu()}
                     {/* <div className="link_box mt-5">     
                        <button>Add</button>
                     </div>            */}
            </div>
             )}
            </AuthUserContext.Consumer>
        );
    }
}

export default withFirebase(ShowMenu);
