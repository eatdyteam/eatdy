import React, { Component } from 'react';
import ShowMenu from './showMenu';
import HeaderSection from '../component/header/HeaderSection/HeaderSection';

class Menu extends Component {
    render() {
        return (
            <div>
                <HeaderSection nameHD='Menu' nameHT='Menu'/>
                <ShowMenu/>
            </div>
        );
    }
}

export default Menu;