/*   start URL for authentication firebase   */
export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const ACCOUNT = '/account';
export const PASSWORD_FORGET = '/pw-forget';
export const ADMIN_DETAILS = '/admin/:id';
export const INFORMATION ='/information';
export const MENU ='/menu';
export const MAP ='/map';

/*   end URL for authentication firebase   */

/*------------------------------------*/

/*   start URL for pages React  */

export const HOME = '/';
export const CONTACT = '/contact';
export const ABOUT = '/about';
export const HEALTH = '/health';
export const FOOD = '/food';
export const NEW = '/new';

/*   end URL for pages React  */

/*------------------------------------*/


/*   start URL for pages users  */
export const USER = '/user';
/*   start URL for pages users  */

/*------------------------------------*/

/*   start URL for pages users  */
export const ADMIN = '/admin';
/*   start URL for pages users  */