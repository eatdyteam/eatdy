import React from 'react';

import { AuthUserContext } from '../component/userLogin/components/Session';

import Header from '../component/header/index';
import HeaderAuth from '../component/header/indexAuth';


const Navigation = () => (
  <AuthUserContext.Consumer>
    {authUser =>
      authUser ? (
        <NavigationAuth  />
      ) : (
        <NavigationNonAuth />
      )
    }
  </AuthUserContext.Consumer>
);

const NavigationAuth = ({authUser }) => (  
  <HeaderAuth/>
);

const NavigationNonAuth = () => (
  <Header/>
);

export default Navigation;
